# SuperTokens helm chart

A simple helm chart to deploy supertokens-core with postgresql using the
official docker images.

## Configuration

The various configuration options and their description can be found in the
values.yaml file.
